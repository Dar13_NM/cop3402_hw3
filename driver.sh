#!/bin/bash
safeRunCommand() {
typeset cmd="$*"
typeset ret_code

echo cmd = $cmd
eval $cmd
ret_code=$?
if [ $ret_code != 0 ]; then
  printf "ERROR\n"
  exit $ret_code
fi
}

command="./scanner.out"
safeRunCommand "$command"

command="./parser.out"
safeRunCommand "$command"

command="./vm.out"
safeRunCommand "$command"

for i
do
  if [ "$i" == "-l" ]
  then
  echo -e "\n"
  cat ./lexemelist.txt
  fi

  if [ "$i" == "-v" ]
  then
  echo -e "\n"
  cat ./stacktrace.txt
  fi

  if [ "$i" == "-a" ]
  then
  echo -e "\n"
  cat ./mcode.txt
  fi
done
