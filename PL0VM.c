////////////////////////////////////
//
// COP 3402- HW#1 PL0 Virtual Machine
//
//
// Authors: Eric Momper  (e3281180)
//
////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_STACK_HEIGHT 2000
#define MAX_CODE_LENGTH 500
#define MAX_LEXI_LEVELS 3

//used to track activation records for printing
int ar[MAX_STACK_HEIGHT];

//Instructions to be read in from mcode.txt
typedef struct
{
	int op;
	int l;
	int m;
}instruction;

//init instruction
instruction* createInstruction(int op,int l, int m)
{
	instruction* instr=NULL;
	instr=calloc(1,sizeof(instruction));
	instr->op=op;
	instr->l=l;
	instr->m=m;
return instr;
}

//return op name for printing
char* opName(int n)
{
	if(n==1)
		return "LIT";
	else if(n==2)
		return "OPR";
	else if(n==3)
			return "LOD";
	else if(n==4)
			return "STO";
	else if(n==5)
			return "CAL";
	else if(n==6)
			return "INC";
	else if(n==7)
			return "JMP";
	else if(n==8)
			return "JPC";
	else if(n==9||n==10||n==11)
			return "SIO";
	else return "";
}

//print individual instrcutions for stacktrace
void printInstr(FILE*fout, int n, char* opName,int l, int m)
{
	fprintf(fout,"%d\t%s\t%d\t%d\t",n, opName,l,m);
}

//print all loaded instructions
void printCode(instruction**code,FILE* fout)
{
	int i=0;
	fprintf(fout,"line\tOP\tL\tM\r\n");
	while(code[i]!=NULL)
		{
		printInstr(fout,i, opName(code[i]->op),code[i]->l,code[i]->m);
		fprintf(fout,"\r\n");
		i++;
		}
}

//print the stack after each exection to the stacktrace
void printStack(FILE* fout,int* stack,int bp,int sp,int pc)
{
	int i=0;
	fprintf(fout,"%d\t%d\t%d\t",pc,bp,sp);
	for(i=1;i<=sp;i++)
		{
			fprintf(fout,"%d ",stack[i]);
			if(ar[i]==1)
				fprintf(fout,"| ");
		}

}

//Find and access parts of diffetrent activation records at various lex levels
int base(int l, int base,int* stack) // l stand for L in the instruction format
{
   int b1; //find base L levels down
  b1 = base;
  while (l > 0)
  {

    b1 = stack[b1 + 1];
    l--;
  }
  return b1;
}

//math ops for opcode 2
int ops(instruction* IR, int* stack,int* sp)
{
	//copy values from top
	if(IR->m==1)
		stack[*sp]=stack[*sp]*-1;

	//add top 2 values on stack, decrement sp
	else if(IR->m==2)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]+stack[*sp+1];
	}

	//subtract top 2 values on stack, decrement sp
	else if(IR->m==3)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]-stack[*sp+1];
	}

	//Multiply top 2 values on stack, decrement sp
	else if(IR->m==4)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]*stack[*sp+1];
	}

	//divide top 2 values on stack, decrement sp
	else if(IR->m==5)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]/stack[*sp+1];
	}

	//MOD top value on stack stack by 2, (even or odd)
	else if(IR->m==6)
	{
		stack[*sp]=stack[*sp]%2;
	}

	//MOD top 2 values on stack, decrement sp
	else if(IR->m==7)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]%stack[*sp+1];
	}

	//decrement sp, assign a==b to a
	else if(IR->m==8)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]==stack[*sp+1];
	}

	//decrement sp, assign a!=b to a
	else if(IR->m==9)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]!=stack[*sp+1];
	}

	//decrement sp, assign a<b to a
	else if(IR->m==10)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]<stack[*sp+1];
	}

	//decrement sp, assign a<=b to a
	else if(IR->m==11)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]<=stack[*sp+1];
	}

	//decrement sp, assign a>b to a
	else if(IR->m==12)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]>stack[*sp+1];
	}

	//decrement sp, assign a>=b to a
	else if(IR->m==13)
	{
		*sp=*sp-1;
		stack[*sp]=stack[*sp]>=stack[*sp+1];
	}

return 0;
}


/*
 Used when returning from another function call
returns PC/ if last return to main returns 0
*/
int ret(instruction* IR,int *stack, int pc,int* sp, int* bp)
{
	//for print
	ar[*bp-1]=0;

	//last activation record return from main
	if(*bp==1)
	{
		*bp=0;
		*sp=0;
			return 0;
	}
	//else go to previous activation record
	*sp=*bp-1;
	pc=stack[*sp+4];
	*bp=stack[*sp+3];
return pc;
}

//print after each exection (prints the stack)
void printExec(instruction* IR, int* stack,int pc, int sp,int bp,FILE* fout)
{

	printStack(fout,stack,bp,sp,pc);
	fprintf(fout,"\r\n");
}

/*
 * called for each exection step
 * returns PC++ or other PC incase of jump, call, or return
 * SIO (11 0 3) will also halt the program unconditionally
 */
int exec(instruction* IR,int *stack, int pc,int* sp, int* bp,FILE* fout)
{
	//print current instruction
	printInstr(fout,pc,opName(IR->op),IR->l,IR->m);
	//increment PC for next cycle
	pc++;

	//load immediate to stack
	if(IR->op==1)
	{
		*sp=*sp+1;
		stack[*sp]=IR->m;
	}

	//perform different ops on stack
	else if(IR->op==2)
	{
		//2 0 0 is return, call ret()
		if(IR->m==0)
			{
				pc=ret(IR,stack,pc,sp,bp);
			}
		//else perform math op in ops()
		else
			ops(IR,stack,sp);
	}

	//Load lower value onto stack
	else if(IR->op==3)
	{
		*sp=*sp+1;
              //printf("%d LOD base(%d %d)= %d  addr %d Stack[%d] \n",pc -1,IR->l,*bp,base(IR->l, *bp, stack) , IR->m,
           //  base(IR->l, *bp, stack) + IR->m );
		stack[*sp]=stack[base(IR->l,*bp,stack)+IR->m];
	}

	//store value into stack, decrement sp
	else if(IR->op==4)
	{
          // printf("%d STO base(%d %d)= %d  addr %d Stack[%d] \n",pc-1, IR->l,*bp,base(IR->l, *bp, stack) , IR->m,
             // base(IR->l, *bp, stack) + IR->m );
		stack[base(IR->l,*bp,stack)+IR->m]=stack[*sp];
		*sp=*sp-1;
	}

	//call sets up new activation record
	else if(IR->op==5)
	{
		//save old AR state to new AR registers
		stack[*sp+1]=0;
		stack[*sp+2]=base(IR->l,*bp,stack);
		stack[*sp+3]=*bp;
		stack[*sp+4]=pc;
		*bp=*sp+1;
		pc=IR->m;

		//print current stack
		printExec(IR,stack,pc,*sp,*bp,fout);

		//for print
		ar[*sp]=1;
	//return early to avoid printing twice
	return pc;
	}

	//inc for reserving new space in stack for local vars and AR vars
	else if(IR->op==6)
	{
		*sp=*sp+IR->m;
	}

	//jump
	else if(IR->op==7)
	{
		pc=IR->m;

	}

	//jump if top of stack equals 0
	else if(IR->op==8)
	{
		if(stack[*sp]==0)
			pc=IR->m;
		*sp=*sp-1;

	}

	//output top of stack, decrement sp
	else if(IR->op==9)
	{
   //printf("in the halt block\n");
    //printf
    if(IR->m == 1)
    {
		printf("OUTPUT: %d\n",stack[*sp]);
		*sp=*sp-1;
	  }

    //scanf
    else if(IR->m == 2)
    {
      *sp=*sp+1;
      printf("Input:\n");
      scanf("%d",(stack+*sp));
    }

    else if(IR->m == 3) 
    {
      printExec(IR,stack,pc,*sp,*bp,fout);
      return 0;
    }
  }
	
	
	printExec(IR,stack,pc,*sp,*bp,fout);

return pc;
}


int main()
{
	//run vm while halt==1
	int halt=1;

	//input vars for fscanf
	int a=0,b=0,c=0;

	//number of instructions
	int nInstr=0;

	//local vars for vm
	int pc=0,sp=0,bp=1;

	int temp=0;
	int i;

	//holds current instruction
	instruction* IR=NULL;

	//files for input and output
	FILE* fin=NULL,*fout=NULL;

	//init array for printing "| " between activation records
	memset(ar,0,sizeof(int)*MAX_STACK_HEIGHT);

	//store instructions
	instruction** code;
	code=calloc(MAX_CODE_LENGTH,sizeof(instruction*));

	//stack for vm data
	int * stack;
	stack=calloc(MAX_STACK_HEIGHT,sizeof(int));

	//files must have these names and be in the same directory
	fin=fopen("mcode.txt","r");
	fout=fopen("stacktrace.txt","w");

	//FILE ERROR
	if(fin==NULL||fout==NULL)
		{
			printf("FILE ERROR\n");
			return 1;
		}

	//Scan in Instructions from mcode.txt
	do
	{
		temp=fscanf(fin,"%d %d %d",&a,&b,&c);
		if(temp!=EOF)
		{
			code[nInstr]=createInstruction(a,b,c);
			nInstr++;
		}
	}while(temp!=EOF);


	//print loaded instructions to stacktrace for 1st instruciton part
	printCode(code,fout);

	//headers for 2nd stacktrace exec part
	fprintf(fout,"\t\t\t\tPC\tBP\tSP\tstack\r\n");
	fprintf(fout,"Initial values\t\t\t%d\t%d\t%d\t\r\n",pc,bp,sp);


	//halt can occur on last return or SIO(11 0 3)
	while (halt==1)
	{
			//fetch
			IR=code[pc];

			//PC incremented or updated in exec stage
			pc=exec(IR,stack,pc,&sp,&bp,fout);

			//halt case
			if(pc==0)
			{
				halt=0;
				free(stack);
				for(i=0;code[i]!=NULL;i++)
					free(code[i]);
				free(code);
			}

	}
	//close files
	fclose(fin);
	fclose(fout);

return 0;
}

