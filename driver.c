////////////////////////////////////
//
// COP 3402 - Homework #3
// Compiler Driver
//
// Authors: Neil Moore   (n3281497)
//          Eric Momper  (e3281180)
//
//Note: assumes there are txt files and .out executables
////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void printFile(FILE* file_in)
{
  if(file_in == NULL)
  {
    return;
  }

  char c;	
  while(fscanf(file_in,"%c",&c)!=EOF)
  {
    printf("%c",c);
  }

  fclose(file_in);
}

int main(int argc, const char** argv)
{
  char* const* argsNone = NULL;
  pid_t child1, child2, child3, tpid;
  int child_status = 0;
	
  child1 = fork();
  if(child1 == 0)
  {
    printf("Running parser\n");
    execv("parser.out",argsNone);	
  }
//parent waiting for child
  else
  {
    tpid = wait(&child_status);
    printf("Done with child1\n");
  }

  //need libpcre.a for scanner.out to make fork for scanner
  /*
  child2 = fork();
  if(child2 == 0)
  {
    printf("running Scanner\n");
    execv("Scanner.out",argsNone);
  }
  //parent waiting for child
  else
  {
    tpid = wait(&child_status);
    printf("Done with child2\n");
  }
*/

  child3 = fork();
  if(child3 == 0)
  {
    printf("running VM\n");
    execv("PL0VM.out",argsNone);
  }
  //parent waiting for child
  else
  {
    tpid = wait(&child_status);
    printf("Done with child3\n");
  }

  //order of command args does not matter printed (lexeme,mcode,stacktrace)
  FILE  *lexeme=NULL, *mcode=NULL, *stacktrace=NULL;
	
  argc--;
  while (argc>0)
  {
    printf("%s\n",argv[argc]);
    if(strcmp(argv[argc],"-l")==0)
      lexeme=fopen("lexemetable.txt","r");
    else if(strcmp(argv[argc],"-a")==0)
      mcode=fopen("mcode.txt","r");
    else if(strcmp(argv[argc],"-s")==0)
      stacktrace=fopen("stacktrace.txt","r");

  argc--;
  }
	
  printf("FILES: %p %p %p\n", mcode, stacktrace, lexeme);
  printFile(lexeme);
  printFile(mcode);
  printFile(stacktrace);

  return 0;
}
