#include "./pcre/pcre.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_IDENT_LEN 11
#define MAX_NUM_LEN 5
#define MAX_TOKENS 50

int err_code = 0x0;

typedef enum
{
  FILE_IO_ERR = 0x1,
  INVALID_IDENT = 0x2,
  INVALID_IDENT_LEN = 0x4,
  INVALID_NUM_LEN = 0x8,
  INVALID_SYMBOLS = 0xF
} error_codes;

typedef enum
{
  nulsym = 1,
  identsym = 2,
  numbersym = 3,
  plussym = 4,
  minussym = 5,
  multsym = 6,
  slashsym = 7,
  oddsym = 8,
  eqlsym = 9,
  neqsym = 10,
  lessym = 11,
  leqsym = 12,
  gtrsym = 13,
  geqsym = 14,
  lparensym = 15,
  rparensym = 16,
  commasym = 17,
  semicolonsym = 18,
  periodsym = 19,
  becomessym = 20,
  beginsym = 21,
  endsym = 22,
  ifsym = 23,
  thensym = 24,
  whilesym = 25,
  dosym = 26,
  callsym = 27,
  constsym = 28,
  varsym = 29,
  procsym = 30,
  writesym = 31,
  readsym = 32,
  elsesym = 33,
  sym_count = 34
} lang_symbols;

const char *reserved_words[13]= {"const","var","procedure","call","begin","end","if",
                                 "then","else","while","do","read","write"};

typedef struct lexeme_type
{
  char lexeme_str[12];
  int type;
  struct lexeme_type* next;
} lexeme;

typedef struct token_type
{
  char tok[11];
  struct token_type* next;
  struct token_type* prev;
} token;

int getLexemeType(char* lexeme);

void cleanCode(FILE* input_file, FILE* output_file);
int isAllWhitespace(char* str);
void clearComments(FILE* input_file, FILE* output_file);

token* pl0_tokenizer(char* str);
lexeme* generateLexemeTable(FILE* input_file);
void generateLexemeList(FILE* input_file);

int isValidSymbol(char symbol_char);

int main(int argc, char** argv)
{
  FILE* input = NULL;
  FILE* cleaner_input = NULL;
  FILE* clean_input = NULL;
  FILE* lexeme_table_out = NULL;
  FILE* lexeme_list_out = NULL;

  lexeme* lexeme_list = NULL;
  lexeme* lexeme_cond = NULL;

  input = fopen("input.txt","r");
  if(input == NULL)
  {
    printf("Input file not found! Please make sure that \"input.txt\" is in the same directory as "
           "this program and try again.\n");
    exit(FILE_IO_ERR);
  }

  cleaner_input = fopen("cleaner_input.txt","w");
  clearComments(input, cleaner_input);
  fclose(cleaner_input);
  cleaner_input = fopen("cleaner_input.txt","r");

  clean_input = fopen("clean_input.txt","w");
  cleanCode(cleaner_input, clean_input);
  fclose(clean_input);

  clean_input = fopen("clean_input.txt","r");
  if(clean_input == NULL)
  {
    printf("There's been an error writing to \'cleaninput.txt\'!\n");
    exit(FILE_IO_ERR);
  }

  lexeme_list = generateLexemeTable(clean_input);

  lexeme_table_out = fopen("lexemetable.txt","w");
  fprintf(lexeme_table_out, "lexeme\ttoken type\n");
  for(lexeme_cond = lexeme_list; lexeme_cond != NULL; lexeme_cond = lexeme_cond->next)
  {
    if(lexeme_cond->type == 1 || lexeme_cond->type == -1) { continue; }
    fprintf(lexeme_table_out, "%s\t%d\n", lexeme_cond->lexeme_str, lexeme_cond->type);
  }

  lexeme_list_out = fopen("lexemelist.txt", "w");
  for(lexeme_cond = lexeme_list; lexeme_cond != NULL; lexeme_cond = lexeme_cond->next)
  {
    if(lexeme_cond->type == 1 || lexeme_cond->type == -1) { continue; }
    fprintf(lexeme_list_out, "%d ",lexeme_cond->type);
    if(lexeme_cond->type == identsym || lexeme_cond->type == numbersym)
    {
      fprintf(lexeme_list_out, "%s ", lexeme_cond->lexeme_str);
    }
  }

  return 0;
}

void clearComments(FILE* input_file, FILE* output_file)
{
  int found_comment = 0;

  char input_char = '\0', next_char = '\0';

  while(!feof(input_file))
  {
    input_char = getc(input_file);

    if(feof(input_file)) { break; }

    if(input_char == '/')
    {
      next_char = getc(input_file);
      if(next_char == '*')
      {
        found_comment = 1;
        continue;
      }
      else
      {
        ungetc(next_char, input_file);
      }
    }

    if(input_char == '*')
    {
      next_char = getc(input_file);
      if(next_char == '/')
      {
        found_comment = 0;
        continue;
      }
      else
      {
        ungetc(next_char, input_file);
      }
    }

    if(found_comment == 0)
    {
      //printf("%c", input_char);
      fwrite(&input_char, sizeof(char), 1, output_file);
    }

  }
}

void cleanCode(FILE* input_file, FILE* output_file)
{
  int found_comment = 0;

  int state = 0; //ALNUM, SYMBOL, NONE(whitespace)

  char input_char = '\0', next_char = '\0';

  while(!feof(input_file))
  {
    input_char = getc(input_file);

    if(feof(input_file)) { break; }

    switch(state)
    {
    case 0: //NONE(whitespace)
      if(isalnum(input_char))
      {
        state = 1;
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);
      }
      else if(isValidSymbol(input_char))
      {
        state = 2;
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);

        if(input_char == ':' || input_char == '<' || input_char == '>')
        {
          next_char = getc(input_file);
          if((input_char == ':' && next_char == '=') || 
             (input_char == '<' && next_char == '>') ||
             ((input_char == '<' || input_char == '>') && next_char == '='))
          {
            //printf("%c", next_char);
            fwrite(&next_char, sizeof(char), 1, output_file);
          }
        }

        //printf(" ");
        fwrite(" ", sizeof(char), 1, output_file);
      }
      else
      {
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);
        state = 0;
      }
      break;
    case 1: //ALNUM
      if(isalnum(input_char))
      {
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);
        state = 1;
        break;
      }
      else if(isValidSymbol(input_char))
      {
        //printf(" %c", input_char);
        fwrite(" ", sizeof(char), 1, output_file);
        fwrite(&input_char, sizeof(char), 1, output_file);
        
        if(input_char == ':' || input_char == '<' || input_char == '>')
        {
          next_char = getc(input_file);
          if((input_char == ':' && next_char == '=') || 
             (input_char == '<' && next_char == '>') ||
             ((input_char == '<' || input_char == '>') && next_char == '='))
          {
            //printf("%c", next_char);
            fwrite(&next_char, sizeof(char), 1, output_file);
          }
          else
          {
            ungetc(next_char, input_file);
          }
        }
        //printf(" ");
        fwrite(" ", sizeof(char), 1, output_file);

        state = 2;
        break;
      }
      else
      {
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);
        state = 0;
        break;
      }
      break;
    case 2: //SYMBOL
      if(isValidSymbol(input_char))
      {
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);

        if(input_char == ':' || input_char == '<' || input_char == '>')
        {
          next_char = getc(input_file);
          if((input_char == ':' && next_char == '=') || 
             (input_char == '<' && next_char == '>') ||
             ((input_char == '<' || input_char == '>') && next_char == '='))
          {
            //printf("%c", next_char);
            fwrite(&next_char, sizeof(char), 1, output_file);
          }
          else
          {
            ungetc(next_char, input_file);
          }
        }
        //printf(" ");
        fwrite(" ", sizeof(char), 1, output_file);

        state = 2;
      } 
      else if(isalnum(input_char))
      {
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);
        state = 1;
      }
      else
      {
        //printf("%c", input_char);
        fwrite(&input_char, sizeof(char), 1, output_file);
        state = 0;
      }
    }
  }
}

int isAllWhitespace(char* str)
{
  int i = 0;
  for(i; i < strlen(str); i++)
  {
    if(str[i] != '\n' || !isspace(str[i]) || str[i] != '\t')
    {
      return 0; 
    }
  }

  return 1;
}

int isValidSymbol(char symbol_char)
{
  int i = 0;
  char valid_symbols[] = {'.',',',';','<','>','=',':','*','/','(',')','+','-'};

  for(; i < 13; i++)
  {
    if(symbol_char == valid_symbols[i])
    {
      return 1;
    }
  }

  return 0;
}

int getLexemeType(char* lexeme)
{
  int idx = 0;

  const char* pcre_err;
  int pcre_err_offset;
  int pcre_ovector[30];
  pcre* isIdentifier = NULL;
  pcre* isIdentifierTooLong = NULL;
  pcre* isNumber = NULL;
  pcre* isNumberTooLong = NULL;

  pcre* invalidIdentifier = NULL;

  int err_detected = 0;

  if(strcmp(lexeme, "var") == 0) { return varsym; }
  if(strcmp(lexeme, "begin") == 0) { return beginsym; }
  if(strcmp(lexeme, "end") == 0) { return endsym; }
  if(strcmp(lexeme, "const") == 0) { return constsym; }
  if(strcmp(lexeme, "procedure") == 0) { return procsym; }
  if(strcmp(lexeme, "call") == 0) { return callsym; }
  if(strcmp(lexeme, "if") == 0) { return ifsym; }
  if(strcmp(lexeme, "then") == 0) { return thensym; }
  if(strcmp(lexeme, "else") == 0) { return elsesym; }
  if(strcmp(lexeme, "while") == 0) { return whilesym; }
  if(strcmp(lexeme, "do") == 0) { return dosym; }
  if(strcmp(lexeme, "read") == 0) { return readsym; }
  if(strcmp(lexeme, "write") == 0) { return writesym; }
  if(strcmp(lexeme, "<") == 0) { return lessym; }
  if(strcmp(lexeme, ">") == 0) { return gtrsym; }
  if(strcmp(lexeme, "=") == 0) { return eqlsym; }
  if(strcmp(lexeme, ":=") == 0) { return becomessym; }
  if(strcmp(lexeme, "<=") == 0) { return leqsym; }
  if(strcmp(lexeme, ">=") == 0) { return geqsym; }
  if(strcmp(lexeme, "<>") == 0) { return neqsym; }
  if(strcmp(lexeme, "(") == 0) { return lparensym; }
  if(strcmp(lexeme, ")") == 0) { return rparensym; }
  if(strcmp(lexeme, ";") == 0) { return semicolonsym; }
  if(strcmp(lexeme, ",") == 0) { return commasym; }
  if(strcmp(lexeme, "+") == 0) { return plussym; }
  if(strcmp(lexeme, "-") == 0) { return minussym; }
  if(strcmp(lexeme, "*") == 0) { return multsym; }
  if(strcmp(lexeme, "/") == 0) { return slashsym; }
  if(strcmp(lexeme, ".") == 0) { return periodsym; }
  if(strcmp(lexeme, "odd") == 0) { return oddsym; }

  /*
  if(isalnum(lexeme[0]) == 0)
  {
    return nulsym;
  }
  */

  isIdentifier = pcre_compile("[A-Za-z]([A-Za-z]|[0-9]){0,10}",
                              0,
                              &pcre_err,
                              &pcre_err_offset,
                              (const unsigned char*)NULL);

  isNumber = pcre_compile("[0-9]{1,5}",
                          0,
                          &pcre_err,
                          &pcre_err_offset,
                          NULL);

  isIdentifierTooLong = pcre_compile("[A-Za-z]([A-Za-z]|[0-9]){11,}",
                                     0,
                                     &pcre_err,
                                     &pcre_err_offset,
                                     NULL);

  isNumberTooLong = pcre_compile("[0-9]{6,}",
                                 0,
                                 &pcre_err,
                                 &pcre_err_offset,
                                 NULL);

  invalidIdentifier = pcre_compile("[0-9]+[A-Za-z]+.*",
                                   0,
                                   &pcre_err,
                                   &pcre_err_offset,
                                   NULL);

  //Check for invalid identifier
  pcre_err_offset = pcre_exec(invalidIdentifier, NULL,
                              lexeme, strlen(lexeme),
                              0, 0,
                              pcre_ovector, 30);
  if(pcre_err_offset >= 0) 
  { 
    printf("Lexical Error: Identifier \"%s\" is invalid!\n", lexeme);
    err_code = err_code | INVALID_IDENT;
    err_detected++;
  }

  //Check for identifier
  pcre_err_offset = pcre_exec(isIdentifier, NULL,
                              lexeme, strlen(lexeme),
                              0, 0,
                              pcre_ovector,30);

  if(pcre_err_offset >= 0) { return identsym; }
  
  //Check number length
  pcre_err_offset = pcre_exec(isNumberTooLong, NULL,
                              lexeme, strlen(lexeme),
                              0, 0,
                              pcre_ovector, 30);
  if(pcre_err_offset >= 0) 
  {
    printf("Lexical Error: Number \"%s\" is too long!\n", lexeme);
    err_code = err_code | INVALID_IDENT_LEN;
    err_detected++;
  }

  //Check if it's a valid number
  pcre_err_offset = pcre_exec(isNumber, NULL,
                              lexeme, strlen(lexeme),
                              0, 0,
                              pcre_ovector, 30);
  if(pcre_err_offset >= 0) { return numbersym; }

  if(lexeme[0] == '\0')
  {
    return nulsym;
  }
 
  if(err_detected == 0)
  {
    printf("Lexical Error: Invalid symbols! Lexeme = %s\n", lexeme);
    err_code = err_code | INVALID_SYMBOLS;
  }

  printf("Lexical Error detected!\n");

  return nulsym;
}

token* pl0_tokenizer(char* str)
{
  if(str == NULL)
  {
    return NULL;
  }

  if(isAllWhitespace(str))
  {
    return NULL;
  }

  char tok[12];
  token* ret_tok = malloc(sizeof(token));
  token* current_tok = ret_tok;
  int str_idx = 0;
  int tok_idx = 0;
  
  int tok_state = 0;

  char *special_symbols[] = {" ","+","-","*","/","(",")","=",":=",",","<",">",">=","<=",";","<>"};

  ret_tok->prev = NULL;
  ret_tok->next = NULL;

  for(str_idx = 0; str_idx < strlen(str); str_idx++)
  {
    char str_char = str[str_idx];
    switch(tok_state)
    {
    case 0: /* TOK_NONE */
      if(isspace(str_char))
      {
        //ignore all whitespace.
        continue;
      }

      if(isValidSymbol(str_char))
      {
        tok_state = 2;
      }

      if(isalnum(str_char))
      {
        tok_state = 1;
      }

      tok[tok_idx] = str_char;
      tok_idx++;
      break;
    case 1: /* TOK_ALNUM */
      if(isalnum(str_char))
      {
        if(tok_idx >= MAX_IDENT_LEN)
        {
          printf("Lexical Error: Identifier length too great!\n");
	        err_code = err_code | INVALID_NUM_LEN;
          exit(err_code);
        }
        tok[tok_idx] = str_char;
        tok_idx++;
      }
      else
      {
        tok_state = 0;
        
        tok[tok_idx] = '\0';
        //printf("%s\n", tok);
        strcpy(current_tok->tok, tok);
        memset(tok, 0, sizeof(char) * 12);
        tok_idx = 0;
        if(!isspace(str_char))
        {
          tok[tok_idx] = str_char;
          tok_idx++;
        }

        if(ispunct(str_char))
        {
          tok_state = 2;
        }

        //move to the next token
        current_tok->next = malloc(sizeof(token));
        current_tok->next->prev = current_tok;
        current_tok->next->next = NULL;
        current_tok = current_tok->next;
        continue;
      }
      break;
    case 2: /* TOK_SYMBOL */
      if(isValidSymbol(str_char) && (str_char == '=' || str_char == '>'))
      {
        tok[tok_idx] = str_char;
        tok_idx++;
      }

      tok_state = 0;

      tok[tok_idx] = '\0';
      tok_idx = 0;
      strcpy(current_tok->tok, tok);
      memset(tok, 0, sizeof(char) * 12);

      if(isValidSymbol(str_char) && (str_char != '=' && str_char != '>'))
      {
        tok[tok_idx] = str_char;
        tok_idx++;

        if(isalnum(str_char))
        {
          tok_state = 1;
        }

      }

      //move to the next token
      current_tok->next = malloc(sizeof(token));
      current_tok->next->prev = current_tok;
      current_tok->next->next = NULL;
      current_tok = current_tok->next;
      break;
    }
    
  }

  tok[tok_idx] = '\0';
  strcpy(current_tok->tok, tok);
  current_tok->next = malloc(sizeof(token));
  current_tok->next->prev = current_tok;
  current_tok->next->next = NULL;
  current_tok = current_tok->next;

  //printf("after tokenizing ret_tok : %p\n", ret_tok);
  //printf("----------TOKENS---------\n");
  //for(current_tok = ret_tok; current_tok != NULL; current_tok = current_tok->next)
  //{
  //  printf("%s\n", current_tok->tok); 
  //}
  //printf("----------TOKENS---------\n");
  return ret_tok;

}

lexeme* generateLexemeTable(FILE* input_file)
{
  int idx_1 = 0;
  int idx_2 = 0;

  lexeme* ret_lex = malloc(sizeof(lexeme));
  lexeme* lex_conductor = ret_lex;
  ret_lex->next = NULL;

  int token_idx = 0;
  char single_line[256] = {'\0'};
  char input_char;
  const char* pcre_err = NULL;
  int pcre_err_offset;
  int pcre_ret_val = 0;
  int pcre_ovector[30];

  token* tokens = NULL;
  token* conductor = NULL;

  int char_idx = 0;

  if(input_file == NULL)
  {
    printf("Error in generateLexemeTable: input_file == NULL\n");
    return;
  }

  //Ensure we're reading from the beginning of the file.
  rewind(input_file);
  while(!feof(input_file))
  {
    memset(single_line, 0, sizeof(char) * 256);
    for(char_idx = 0; char_idx < 256; char_idx++)
    {
      input_char = getc(input_file);
      if(!isalnum(input_char) && !isspace(input_char) && !ispunct(input_char))
      {
        continue;
      }
      if(input_char == '\n' /*|| input_char == 0xD*/)
      {
        input_char = getc(input_file);
        if(input_char != '.') { ungetc(input_char, input_file); }
        single_line[char_idx] = '\0';
        break;
      }
      else
      {
        if(input_char == '\t' || input_char == ' ')
        {
          input_char = ' ';
        }
        single_line[char_idx] = input_char;
      }
    }

    //have single line of code now.
    tokens = pl0_tokenizer(single_line);
    if(tokens != NULL)
    {
      conductor = tokens;
    }
    else
    {
      conductor = NULL;
    }

    while(conductor != NULL)
    {
      token* tmp = NULL;
      if(conductor->tok[0] == '\0')
      {
        //remove this token.
        tmp = conductor->next;
        if(tmp == NULL)
        {
          conductor = NULL;
          continue;
        }
        conductor = conductor->prev;
        free(conductor->next);
        conductor->next = tmp;
        tmp->prev = conductor;
        conductor = tmp;
      }
      
      strcpy(lex_conductor->lexeme_str, conductor->tok);
      lex_conductor->type = getLexemeType(conductor->tok);
      lex_conductor->next = malloc(sizeof(lexeme));
      lex_conductor->next->next = NULL;
      lex_conductor = lex_conductor->next;
      lex_conductor->type = -1;

      conductor = conductor->next;
    }
  }

  return ret_lex;
}
