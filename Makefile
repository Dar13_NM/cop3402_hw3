#took out driver.out
all: scanner.out parser.out vm.out 
clean: 
	rm *.out

#scanner
scanner.out: scanner.o
	-gcc -o scanner.out scanner.o ./scanner/pcre/x86_64/libpcre.a
	rm *.o

#parser	
parser.out: parser.o
	-gcc -g -rdynamic -o parser.out parser.o
	rm *.o

#VM
vm.out: PL0VM.o
	-gcc -o vm.out PL0VM.o
	rm *.o

#driver
#driver.out: driver.o
#	-gcc -o driver.out driver.o
#	rm *.o


	
#dependencies for Scanner Use Neil's Scanner?
scanner.o: ./scanner/scanner.c
	gcc -c ./scanner/scanner.c

#dependencies for Parser
parser.o: parser.c
	gcc -g -rdynamic -c parser.c

#dependencies for VM
PL0VM.o: PL0VM.c
	gcc -c PL0VM.c
	
#dependencies for Driver
#driver.o: driver.c
#	gcc -c driver.c
