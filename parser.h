#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CODE_LENGTH 500
#define symTableLength 100

//Instructions to be written to mcode.txt
//for code (fixed array size 500 )
typedef struct
{
  int op;
  int l;
  int m;
}instruction;

//for lexemes (linked list)
typedef struct symbol
{
  int kind;
  char name[12];
  int val;
}symbol;

typedef struct list
{
  struct list* next;
  symbol* data;
}list;

//for symbol table (set size 100)
typedef struct variable
{
  int kind;       // const = 1, var = 2, proc = 3
  char name[12];  // name up to 11 chars
  int val;        // number (ASCII value)
  int level;      // L level
  int addr;       // M address
} variable;

typedef enum
{ 
  nulsym = 1,
  identsym = 2,
  numbersym = 3,
  plussym = 4,
  minussym = 5,
  multsym = 6,
  slashsym = 7,
  oddsym = 8,
  eqlsym = 9,
  neqsym = 10,
  lessym = 11,
  leqsym = 12,
  gtrsym = 13,
  geqsym = 14,
  lparensym = 15,
  rparensym = 16,
  commasym = 17,
  semicolonsym = 18,
  periodsym = 19,
  becomessym = 20,
  beginsym = 21,
  endsym = 22,
  ifsym = 23,
  thensym = 24,
  whilesym = 25,
  dosym = 26,
  callsym = 27,
  constsym = 28,
  varsym = 29,
  procsym = 30,
  writesym = 31,
  readsym = 32,
  elsesym = 33,
  sym_count = 34
} lang_symbols;

typedef enum
{
  lit=1,
  opr=2,
  lod=3,
  sto=4,
  cal=5,
  inc=6,
  jmp=7,
  jpc=8,
  sio=9
}ops;

typedef enum
{
  opRet=0,
  opNeg=1,
  opAdd=2,
  opSub=3,
  opMult=4,
  opDiv=5,
  opOdd=6,
  opMod=7,
  opEql=8,
  opNeq=9,
  opLss=10,
  opLeq=11,
  opGtr=12,
  opGeq=13
}opVal;
