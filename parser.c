////////////////////////////////////
//
// COP 3402 - Homework #4
// Parser / Code Generation
//
// Authors: Neil Moore   (n3281497)
//          Eric Momper  (e3281180)
//
////////////////////////////////////
#include "parser.h"


//error flag
int error=0;

//current token
symbol* token = NULL;

//instruction array of size 500
instruction code[MAX_CODE_LENGTH];
int codePtr = 0;

//LexLevel Pointer
int level = 0;

//symbol table array of size 100
variable symTable[symTableLength];
int symTablePtr = 0;
int varCount = 0;
int constCount = 0;

//linked list of tokens
list* listHead = NULL;

//create node for linked list
list* createNode(int lexeme, char* str,int val)
{
  list* temp = calloc(1,sizeof(list));
  temp->data = calloc(1,sizeof(symbol));
  if(strcmp(str,"") != 0)
  {
    strcpy(temp->data->name,str);
  }

  temp->data->kind = lexeme;
  temp->data->val = val;
  return temp;
}


//add token to list
list* addList(list* head, int lexeme, char* str, int val)
{
  if(head == NULL)
  {
    return createNode(lexeme, str,val);
  }

  //else add to list
  list* temp = head;
  while(temp->next != NULL)
  {
    temp = temp->next;
  }

  temp->next = createNode(lexeme, str,val);
  return head;
}

//Replace with pow() from standard library.
int power(int n, int p)
{
  if(p == 0)
  {
    return 1;
  }
  return n * power(n,p-1);
}

//pull numbers from file
int parseNum(char* str)
{
  int i = 0, length = 0, num = 0, p = 0;
  if(str[0]-'0' < 0 || str[0]-'0' > 9)
  {
    return -1;
  }

 for(i = 0; str[i] != '\0'; i++)
  {
    length++;
  }

  for(i = length-1; i >= 0; i--)
  {
    num += (str[i]-'0') * power(10,p);
    p++;
  }

  return num;
}

//create list of tokens
list* buildTokens(list *head)
{
  FILE* lexeme = fopen("lexemelist.txt","r");
  char* word;
  char *str;
  
  word = calloc(12,sizeof(char));
  int i = 0;
  int j = 0;
  int num = 0;

  while(fscanf(lexeme,"%s",word) != EOF)
  {
    j = parseNum(word);
    if(j == identsym)
    {
      fscanf(lexeme,"%s",word);
      head = addList(head,j,word,0);
    }
    else
    { 
      if(j == numbersym)
      {
        fscanf(lexeme,"%s",word);
        num = parseNum(word);
        head = addList(head,j,"",num);
      }
      else
      {
        head=addList(head,j,"",0);
      }
    }
  }

  fclose(lexeme);
  return head;
}

void printList(list* head)
{
  list* temp = NULL;

  //header
  temp = head;
  while(temp != NULL)
  {
    printf("%d %d %s\n", temp->data->kind, temp->data->val, temp->data->name);
    temp = temp->next;
  }
}

void printCode(FILE* mcode)
{
  int i=0;
  while(i<codePtr)
  {
    fprintf(mcode,"%d %d %d\n",code[i].op,code[i].l,code[i].m);
    i++;
  }
}

//return -1 if not found index?
int find(char* name)
{
  int i = 0;
  //printf("Finding symbol: %s\n", name);
  for(i = symTablePtr; i >= 0; i--)
  {
    //printf("\tsymbol: %s level: %d addr: %d\n", symTable[i].name, symbolLevel(i), symbolAddress(i));
    if(strcmp(name,symTable[i].name)==0)
  	  return i;
  }

  return -1;
}
//access functions
int symbolType(int index)
{
  return symTable[index].kind;
}

int symbolLevel(int index)
{
  return symTable[index].level;
}

int symbolAddress(int index)
{
  return symTable[index].addr;
}

int symbolVal(int index)
{
  return symTable[index].val;
}

//add var or const to symbol table
void addVar(int kind, char* name,int val, int addr)
{
  symTable[symTablePtr].kind = kind;
  symTable[symTablePtr].level = level;
  strcpy(symTable[symTablePtr].name,name);
  symTable[symTablePtr].val = val;
  symTable[symTablePtr].addr = addr;
  symTablePtr++;
}

//create instruction for code array
void gen(int op, int l, int m)
{
  //if(l == 1) { m++; }

  code[codePtr].op = op;
  code[codePtr].l = l;
  code[codePtr].m = m;
  codePtr++;
}


//fifo to get first token from front of list since insertion is done at the end
void getToken()
{
  if(listHead == NULL)
    return;

  list* temp = listHead;
  listHead = listHead->next;
  symbol* sym = temp->data;
  free(temp);
  token = sym;
 // printf("Getting token %d\n",token->kind);
}

symbol* peekToken()
{
  if(listHead == NULL)
  {
    return NULL;
  }

  return listHead->next->data;
}

void program();
void block();
void constDeclaration();
void varDeclaration();
void procDeclaration();
void ifStatement();
void whileLoop();
void statement();
void condition();
void relationOperation();
void expression();
void term();
void factor();

int main(int argc, char** argv)
{	

  FILE* mcode = fopen("mcode.txt","w");
  if(mcode == NULL)
  {
    printf("Error opening mcode.txt!\n");
    return -1;
  }
  listHead = buildTokens(listHead);
  program();
  printCode(mcode);
  fclose(mcode);
  return error;
}

//contains block and .
void program()
{
  block();
  getToken();
  if(token->kind != periodsym)
  {
    error=1;
    printf("Error: period expected.\n");
    exit(1);
  }
}

//contains varDec, constDec, statements
void block()
{
  int prevSymTablePtr = symTablePtr;
  level++;
  int space = 4;
  int jumpAddr = 0;
  if(level == 0)
    gen(jmp, 0, codePtr);
  else
    gen(jmp, 0, codePtr+1);
  jumpAddr=codePtr;
  getToken();

  if(token->kind == constsym)
  {
    constDeclaration();
    getToken();
  }

  varDeclaration();

  int oldVar = varCount;
  varCount = 0;

  code[jumpAddr].m = 1;

  getToken();
  while(token->kind == procsym)
  {
    procDeclaration();
    getToken();
    code[jumpAddr].m = codePtr;
  }

  gen(inc,0,oldVar);
  gen(inc, 0, 4);

  statement();
  symTablePtr = prevSymTablePtr - 1;
  gen(opr, 0,0 );
  level--;

/*dont need to halt any more, vm stops on ret
  if(token->kind == periodsym)
  {
    gen(sio, 0, 3); 
  }
*/
}
void procDeclaration()
{
 // printf("start ProcDecl\n");
  getToken();
  if(token->kind == identsym)
  {
    addVar(3, token->name, 0, codePtr + 1);
    getToken();
    if(token->kind == semicolonsym)
    {
      block();
      if(token->kind != semicolonsym)
      {
        printf("Error: Semicolon or end expected.\n");
        exit(1);
      }
    }
    else
    {
      printf("Error: Incorrect symbol after procedure declaration.\n");
      exit(1);
    }
  }
  else
  {
    printf("Error: procedure must be followed by an identifier.\n");
  }
//  printf("end ProcDecl\n");
}


//adds vars to symbol table
void varDeclaration()
{
 // printf("start VarDecl token: %d\n", token->kind);
  if(token->kind == varsym)
  {
    do
    {
      getToken();
      if(token->kind == identsym)
      { 
      //  printf("token content = %s\n", token->name);
      //  printf("current level = %d\n", level);
        addVar(token->kind,token->name,0,varCount);
        varCount++;
        getToken();
      }
      else
      {
        error=1;
        //error ident expected
        printf("Error: Var must be followed by identifier.\n");
        exit(1);
      } 
    }while(token->kind == commasym);
    
    if(token->kind != semicolonsym)
    {
      error=1;
      //error semicolon expected
      printf("Error: Semicolon or comma missing.\n");
      exit(1);
    }
  }

//  printf("end VarDecl\n");
}

//adds consts to symbol table
void constDeclaration()
{
  int temp = 0;
  char* tempName = NULL;
  if(token->kind == constsym)
  {
    do
    {
      getToken();
      if(token->kind == identsym)
      {
        temp = token->kind;
        tempName = token->name;
        getToken();
        if(token->kind == eqlsym)
        {
          getToken();
        }
        else 
        {
          error=1;
          //error equal sign expected
          printf("Error: Identifier must be followed by =.\n");
	  exit(1);        
	}

        if(token->kind == numbersym)
        {
          //consts are never put in stack
          addVar(1,tempName,token->val, -1);
          constCount++;
        }
        else
        {
          error=1;
          //error number expected
          printf("Error: = must be followed by a number.\n");
	  exit(1);        
	}
      }
      else
      {
        error=1;
        //error identifier expected
        printf("Error: Const must be followed by identifier.\n"); 
	exit(1);
      }
      getToken();
    }while(token->kind == commasym);

    if(token->kind != semicolonsym)
    {
      error=1;
      printf("Error: Semicolon or comma missing.\n"); 
    }
  }
}

//handles multiple statements and check for ; followed by end.
void begin()
{
 // printf("start Begin\n");
  if(token->kind == beginsym)
  {
    getToken();
    statement();
    while(token->kind == semicolonsym || 
          token->kind == beginsym || 
          token->kind == ifsym || 
          token->kind == whilesym || 
          token->kind == writesym || 
          token->kind == readsym)
    {
      if(token->kind == semicolonsym)
      {
        getToken();
        statement();
       // printf("begin - after statement token: %d\n", token->kind);
      }
      else 
      {
        error=1;
        //error, missing semicolon
        printf("Error: Semicolon between statements missing.\n");
	      exit(1);
      }
    }

    if(token->kind == endsym)
    {
      //end symbol
      getToken();
      if(token->kind == semicolonsym)
      {
      }
      else if(token->kind != endsym && token->kind != periodsym)
      {
        statement();
      }
    }
    else
    {
      error=1;
      printf("Error: End symbol expected.\n");
      exit(1);
    }
  }
//  printf("end Begin\n");
}

//if followed by condition and then and statements
// can be begin >> end scope of statements 
void ifStatement()
{
  symbol* peek = NULL;
  int temp = 0;
  int temp2 = 0;
  condition();
  if(token->kind != thensym)
  {
    error=1;
    //error then
    printf("Error: 'then' expected.\n");
    exit(1);
  }
  else
  {
    getToken();
   // printf("ifStatement token into statement: %d\n", token->kind);
    temp = codePtr;
    gen(jpc,0,0);
    statement();
    gen(jmp,0,0);
    temp2 = codePtr;
    code[temp].m = codePtr;

    //else
    //getToken();
    peek = peekToken();
    if(peek == NULL)
    {
      printf("Internal memory error\n");
      exit(2);
    }
    else
    {
      if(peek->kind == elsesym)
      {
        getToken();
      }
    }

    if(token->kind == elsesym)
    {
      getToken();
      statement();

      code[temp2 - 1].m = codePtr;
    }
    else
    {
      code[temp2 - 1].m = temp2;
    }
  }

}

//while followed by condition and do and statements, 
//can be begin >>end scope of statements
void whileLoop()
{
  int temp = 0,temp2 = 0;
  temp = codePtr;
  condition();
  temp2 = codePtr;
  gen(jpc,0,0);
  if(token->kind != dosym)
  {
    error=1;
    //error do expected
    printf("Error: 'do' expected.\n");
    exit(1);
  }
  else
  {
    getToken();
    statement();
    gen(jmp, 0, temp);
    code[temp2].m = codePtr;
  }
}

// can have begin, while, if, statement, read, or write 
void statement()
{
  int i;
  //end
  if(token->kind == endsym)
  {
    return;
  }
  //assignment statement
  else if(token->kind == identsym)
  {
    //spec uses sentinel control where find puts key in 0 index retuns 0 if no match
    //this version of find returns -1 if it is not found
    i=find(token->name);
    if(i == -1)
    {
      error=1;
      //error undeclared var
      printf("Error: Undeclared identifier, '%s'\n", token->name);
      exit(1);
    }

    //var type 
    if(symbolType(i) != 2)
    {
      error=1;
      //error cant assign to const
      printf("Error: Assignment to constant or procedure is not allowed.\n");
      exit(1);
    }

    getToken();
    if(token->kind == becomessym)
    {
      getToken();
      expression();
      //printf("Storing - current level: %d rymbol @ addr %d\n", level, symbolAddress(i) + 4);
      gen(sto, level - symbolLevel(i),symbolAddress(i) + 4);
      //printf("\taddress generated: %d\n", code[codePtr - 1].m);
    }
    else
    {
      error=1;
      //error := expected
      printf("Error: Assignment operator expected.\n");
      exit(1);
    }

  }
  //begin, repeats scope
  else if(token->kind == beginsym)
  {
    begin();
  }
  //if
  else if(token->kind == ifsym)
  {
    getToken();
    ifStatement();
  }
  //write
  else if(token->kind == writesym)
  {
    getToken();
    i = 0;
    if(token->kind == identsym)
    {
      i = find(token->name);
      if(i == -1)
      {
        error=1;
        printf("Error: Undeclared identifier\n");
        exit(1);
      }
      else
      {
        gen(lod, level -symbolLevel(i), symbolAddress(i) + 4);
      }
    }
    if(i >= 0)
    {
      gen(sio, 0, 1);
    }
    if(token->kind == endsym) return;
    getToken();
  }

  //call is in the right place!!! still in progress
  else if(token->kind ==callsym)
  {
    getToken();
    if(token->kind != identsym)
    {
      //error
      printf("Error: Identifier Expected.\n");
      exit(1);
    }
    i = find(token->name);
    if(i == -1)
    {
      printf("Error: Undeclared Identifier.\n");
      exit(1);
    }
    if(symbolType(i) == 3)
    {
      gen(cal, level - symbolLevel(i), symbolAddress(i));
    }
    else
    {
      printf("Error: Call must be followed by a procedure identifier.\n");
      exit(1);
    }
    
    getToken();
  }

  //read
  else if(token->kind ==  readsym)
  {
    getToken();
    i=0;
    if(token->kind == identsym)
    {
      i=find(token->name);
      if(i== -1)
      {
        error=1;
        printf("Error: Undeclared identifier\n");
        exit(1);
      }
      else 
      {
        gen(sio, 0,2);
        gen(sto, level - symbolLevel(i), symbolAddress(i) +4);
      }
    }
  getToken();
  }
  //while
  else if(token->kind == whilesym)
  {
    getToken();
    whileLoop();
  }
  else if(token->kind == semicolonsym)
  {
    getToken();
  }
  else
  {
    error=1;
    //error
    printf("Error: Statement expected. %d\n",token->kind);
    exit(1);
  }
}

//evaluates condition in for  if and while
void condition()
{
  if(token->kind == oddsym)
  {
    getToken();
    expression();
    gen(opr,0,opOdd);
  }
  else
  {
    expression();
    relationOperation();
  }
}

//compares condition expressions
void relationOperation()
{
  if(token->kind>=9 && token->kind<=14)
  {
 
    int temp=0;  
    temp=token->kind;

    getToken();
    expression();
    //enums math_op enum matches if token->kind -1
    gen(opr,0,temp-1);
    return;
  }
  else
  {
    error=1;
    //error
    printf("Error: Relational operator expected.\n");
    exit(1);
  }
}

//is called by statement + or - with term that calls factor
void expression()
{
  int math_op;
  if(token->kind == plussym||token->kind == minussym)
  {
    math_op=token->kind;
    getToken();
    term();
    if(math_op == minussym)
      gen(opr,0,opNeg);
  }
  else
  {
    term();
  }

  while(token->kind == plussym||token->kind == minussym)
  {
    math_op=token->kind;
    getToken();
    term();
    if(math_op == plussym)
    {
      gen(opr,0,opAdd);
    }
    else
    {
      gen(opr,0,opSub);
    }
  }
}

//handles mult or divide and factors
void term()
{
  int math_op;
  factor();
  while(token->kind == multsym || token->kind == slashsym)
  {
    math_op = token->kind;
    getToken();
    factor();

    if(math_op == multsym)
    {
      gen(opr, 0, opMult);
    }
    else
    {
      gen(opr, 0, opDiv);
    }
  }
}

//handles factors and parenthesized expressions
void factor()
{
  int i;
  if(token->kind == identsym)
  {
    i=find(token->name);

    if(i==-1)
    {
      error=1;
      //error undeclared var
      printf("Error: Undeclared identifier, '%s'\n", token->name);
      exit(1);
    }
    else if(symbolType(i) == 2)
    {
      gen(lod, level - symbolLevel(i),symbolAddress(i) + 4);
    }
    else if(symbolType(i) == 1)
    {
      gen(lit,0,symbolVal(i));
    }
    else
    {
      error=1;
      //error
      //This might not be the correct error message.
      printf("Error: The proceeding factor cannot begin wiht this symbol.\n");
      exit(1);
    }
    getToken();
  }
  else if(token->kind == numbersym)
  {
    gen(lit, 0, token->val);
    getToken();
  }
  else if(token->kind == lparensym)
  {
      getToken();
      expression();
      if(token->kind != rparensym)
      {
        error=1;
        //error
        printf("Error: Right parenthesis missing.\n");
        exit(1);
      }
      getToken();
   }
  //relational operator for condition
  else if(token->kind == eqlsym || token->kind == neqsym || token->kind == lessym 
          || token->kind == leqsym || token->kind == gtrsym ||token->kind == geqsym)
  {
    return;
  }
  else
  {
    error=1;
   //error 
   printf("Error: The preceding factor cannot begin with this symbol.\n");
   exit(1);
  }

}

